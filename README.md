# usacoviddeaths.com 
usacoviddaths.com was a very small web application written in python flask. It queried Covid-19 death statistics from [https://api.covidtracking.com/](https://api.covidtracking.com/) and built an infographic-style page to visualize that number against the death tolls of 9/11, Titanic and Pearl Harbor. Its purpose was not to be morbid or trivialize any of the involved events: rather, it was meant to help visitors compehend the intense gravity and seriousness of the pandemic in a simple, visually striking way that could be quickly and easily consumed.

|Screenshot 1 |Screenshot 2  |Screenshot 3 |
--- | --- | ---
|![Screenshot](./images/screenshot.png)|![Screenshot](./images/screenshot2.png)|![Screenshot](./images/screenshot3.png)|

In addition to its communicative mission, it served the double purpose of a practical, real world tutorial in Azure Application Service: building, configuring, securing and maintaining.

Although the project has since been removed from the live web, the code is preseved here (with Google Analytics code removed). A capture from when the project was live is also available for perusal at the [Internet Archive](https://web.archive.org/web/20210330005555/https://usacoviddeaths.com/).

Highlights:
 - Visually striking, minimalist infographic style for maximum impact and speed of communication
 - Extremely simple, minimal code was quick to build and ensured reliability
 - Utilized the flask-talisman package to provide security headers and a secure platform for serving the flask application
  - Hosted securely on Azure Application Service
  - Served as a practical, real-world study in building and securing python web apps as well as the configuration, securing and maintenance of live Azure PaaS web applications.
