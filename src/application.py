from flask import Flask, render_template
from flask_talisman import Talisman
import requests
import json
import datetime
import dateutil.parser

app = Flask(__name__)

csp = {
    'default-src': [
        '\'self\''
    ],
    'script-src': [
        '\'self\''
    ],
    'style-src':[
        '\'self\'',
        'fonts.googleapis.com'
    ],
    'font-src':[
        'fonts.gstatic.com'
    ],
    'image-src':[
        '\'self\''
    ],
    'frame-src':[
        '\'none\''
    ],
    'frame-ancestors':[
        '\'none\''
    ],
    'connect-src':[
        '\'self\''
    ]
}
talisman = Talisman(app, content_security_policy=csp)

@app.route('/')
def index():
    r = requests.get('https://api.covidtracking.com/v1/us/current.json')
    data = r.json()
    
    nine_eleven_count = int (data[0]['death']/2977)
    pearl_harbor_count = int (data[0]['death']/2403)
    titanic_count = int (data[0]['death']/1496)
    
    nine_eleven_dict = dict.fromkeys(range(int (data[0]['death']/2977)))
    titanic_dict = dict.fromkeys(range(int (data[0]['death']/1496)))
    pearl_harbor_dict = dict.fromkeys(range(int (data[0]['death']/2403)))

    check_date = dateutil.parser.isoparse(data[0]['dateChecked'])
    date_string = "%s %i, 2021" % (check_date.strftime('%B'), check_date.day)

    return render_template('index.html', date=date_string, deaths=data[0]['death'], 
        nineElevens=nine_eleven_count, nine_eleven_dict=nine_eleven_dict, 
        pearl_harbors=pearl_harbor_count, pearl_harbor_dict=pearl_harbor_dict, 
        titanics=titanic_count, titanic_dict=titanic_dict)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')